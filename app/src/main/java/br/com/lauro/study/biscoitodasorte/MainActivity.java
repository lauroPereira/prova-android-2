package br.com.lauro.study.biscoitodasorte;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.TaskStackBuilder;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;

import android.support.v4.app.NotificationCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.net.HttpURLConnection;
import java.net.URL;

public class MainActivity extends AppCompatActivity {

    private static String mensagem;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        startService(new Intent(this, RecadoService.class));
        final Button btn = findViewById(R.id.btn);
        //msgCampo = (TextView) findViewById(R.id.mensagem);

        if( this.getIntent().getExtras() != null && !this.getIntent().getExtras().isEmpty() &&
                this.getIntent().getExtras().getString("mensagen_service") != null &&
                !this.getIntent().getExtras().getString("mensagen_service").isEmpty()){
            mensagem = this.getIntent().getExtras().getString("mensagen_service");
        }

        btn.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                busca(v);
                TextView field = (TextView) findViewById(R.id.mensagem);
                field.setText(mensagem);

                if(mensagem != null){
                    if(!mensagem.equals("Sem mensagem")){
                        notificar(mensagem);
                    }
                }
            }
        });
    }

    public void busca(View v){
        String url = "http://biscoito.herokuapp.com";
        new DownloadWebpageTask().execute(url);
    }

    private class DownloadWebpageTask extends AsyncTask<String, Void, String> {

        @Override
        protected String doInBackground(String... urls) {
            try {
                return downloadUrl(urls[0]);
            } catch (IOException e) {
                return "Unable to retrieve web page. URL may be invalid." + e;
            }
        }

        @Override
        protected void onPostExecute(String result) {
            Log.d("Debug", result);
            JSONObject jObject = null;
            try {
                jObject = new JSONObject(result);
            } catch (JSONException e) {
                e.printStackTrace();
            }
            String text = null;
            try {
                text = jObject.getString("mensagem");
            } catch (JSONException e) {
                e.printStackTrace();
            }
            mensagem =text;

        }
    }

    private String downloadUrl(String myurl) throws IOException {
        InputStream is = null;

        int len = 500;
        Log.d("DEBUG", "url: " + myurl);

        try {
            URL url = new URL(myurl);
            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            conn.setReadTimeout(10000 /* milliseconds */);
            conn.setConnectTimeout(15000 /* milliseconds */);
            conn.setRequestMethod("GET");
            conn.setDoInput(true);
            // Starts the query
            conn.connect();
            int response = conn.getResponseCode();
            Log.d("DEBUG", "Resposta HTTP: " + response);
            is = conn.getInputStream();

            String contentAsString = readIt(is);
            return contentAsString;

        } finally {
            if (is != null) {
                is.close();
            }
        }
    }

    public String lerDadosDeStream(InputStream stream, int len) throws IOException {
        Reader reader = null;
        reader = new InputStreamReader(stream, "UTF-8");
        char[] buffer = new char[len];
        reader.read(buffer);
        return new String(buffer);
    }

    private static String readIt(InputStream stream) throws IOException {
        Reader reader = null;
        StringBuffer buffer = new StringBuffer();//Objeto de que vai armazenar o resultado
        reader = new InputStreamReader(stream, "UTF-8"); //Objeto leitor
        Reader in = new BufferedReader(reader); //Converte de input em buffer
        int ch;
        while ((ch = in.read()) > -1) {//Lendo Char por char
            buffer.append((char) ch);
        }
        in.close();
        String ret = new String(buffer);
        return ret;
    }


    private void notificar(String msg) {
        Toast.makeText(getApplicationContext(), msg, Toast.LENGTH_SHORT).show();
        NotificationCompat.Builder mBuilder =
                new NotificationCompat.Builder(this)
                        .setSmallIcon(R.drawable.ic_launcher_background)
                        .setContentTitle("Lucky")
                        .setContentText(msg);

        Intent resultIntent = new Intent(this, MainActivity.class);
        this.getIntent().putExtra("mensagen_service", msg);

        TaskStackBuilder stackBuilder = TaskStackBuilder.create(this);

        stackBuilder.addParentStack(MainActivity.class);

        stackBuilder.addNextIntent(resultIntent);
        PendingIntent resultPendingIntent =
                stackBuilder.getPendingIntent(
                        0,
                        PendingIntent.FLAG_ONE_SHOT
                );
        mBuilder.setContentIntent(resultPendingIntent);
        NotificationManager mNotificationManager =
                (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        mNotificationManager.notify(1, mBuilder.build());
    }
}
